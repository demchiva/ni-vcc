let http = require('http');

http.createServer((req, res) => {
    res.end("Hello " + req.url.slice(1));
}).listen(8080, function () {
    console.log('server started on port 8080');
});
